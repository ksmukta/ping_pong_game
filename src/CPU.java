import java.awt.Color;

public class CPU extends Player {

	public CPU(int position,String name, int i) {
		super(position, name, i);
		isCPU = true;
		isConnected = false;
		toSend = false;
	}

	public static int Strikeposition;		//distance of strike
	public static int FakeStrikePos;		//which CPU player will move fakely
	public static int moveDirection;		//+1 or -1
	public static int playerToMove,realMoveDirection;
	public static int delta = 50;
	public int playerSpeed = Player.playerSpeed/2;
	Color color;
	public void move(){
		if(i==playerToMove){
			setRealMoveDirection();
			int tempPosition = position + (realMoveDirection)*playerSpeed;
			if(tempPosition+size/2>=game.getWidth()-playerWidth){
				position = game.getWidth()-playerWidth - size/2;
			}else if(tempPosition-size/2<=playerWidth){
				position = playerWidth + size/2;
			}else{
				position = tempPosition;
			}
		}else if(i==FakeStrikePos){
			int tempPosition = position + (moveDirection)*(playerSpeed/2);
			if(tempPosition+size/2>=game.getWidth()-playerWidth){
				position = game.getWidth()-playerWidth - size/2;
			}else if(tempPosition-size/2<=playerWidth){
				position = playerWidth + size/2;
			}else{
				position = tempPosition;
			}
		}else{
			if(!(Math.abs(Strikeposition - position) <= delta)){
				moveToCenter();
				position += myMoveDirection*(playerSpeed/2);
			}
		}
	}
	
	public int myMoveDirection;
	public void moveToCenter(){
		if(position < Strikeposition){
			myMoveDirection = 1;
		}else if(position > Strikeposition){
			myMoveDirection = -1;
		}
	}
	
	public void setRealMoveDirection(){
		if(Math.abs(Strikeposition - position) <= delta){
			realMoveDirection = 0;
		}else if(position < Strikeposition){
			realMoveDirection = 1;
		}else if(position > Strikeposition){
			realMoveDirection = -1;
		}
	}
	
	public static int intersection(int wall){
		int final_wall = -1;
	 
		 if(wall%4 == 0 && game.ball[0].xa >0){
			 if(game.ball[0].theta < Math.atan(600.0/(600-game.ball[0].x))){
				 final_wall = 1;
				 Strikeposition = (int)((600.0-game.ball[0].x)*(Math.tan(game.ball[0].theta)));
				 
				 if(Math.atan(600.0/(600-game.ball[0].x))/2.0 < game.ball[0].theta){
					 FakeStrikePos = 2;
					 moveDirection = 1;
				 }
			 } else {final_wall = 2;
			  Strikeposition = (int) (game.ball[0].x + (600.0/(Math.tan(game.ball[0].theta))));
			  
			  if(game.ball[0].theta < (3.0/2.0)*Math.atan(600.0/(600-game.ball[0].x))){
				  FakeStrikePos = 1;
				  moveDirection = 1;
			  }
			 }
		 }
		 
		 
		 else if(wall%4 == 0 && game.ball[0].xa <0){
			 if( (180*(Ball.degToRad) - game.ball[0].theta) < Math.atan(600.0/game.ball[0].x) ){
				 final_wall = 3;
				 Strikeposition = -(int)(game.ball[0].x*(Math.tan(game.ball[0].theta)));
				 if(Math.atan(600.0/game.ball[0].x)/2.0 < (180*(Ball.degToRad) - game.ball[0].theta)){
					 FakeStrikePos = 2;
					 moveDirection = -1;
				 }
				 
			 }
			 else {final_wall = 2;
			 Strikeposition = (int) (game.ball[0].x + (600.0/(Math.tan(game.ball[0].theta))));
			 
			 if((180*(Ball.degToRad) - game.ball[0].theta) < (3.0/2.0)*(Math.atan(600.0/game.ball[0].x))){
				 FakeStrikePos = 3;
				 moveDirection = 1;
			 }
			 }
		 }
		 
		 
		 else if(wall%4 == 1 && game.ball[0].ya >0){
			 
			 if((game.ball[0].theta - (90*Ball.degToRad)) < Math.atan(600.0/(600-game.ball[0].y))){
				 final_wall = 2;
				 Strikeposition = (int) (600.0 - (game.ball[0].y - 600.0)/(Math.tan(game.ball[0].theta)));
				 
				 if(Math.atan(600.0/(600-game.ball[0].y))/2.0<(game.ball[0].theta - (90*Ball.degToRad))){
					 FakeStrikePos = 3;
					 moveDirection = 1;
				 }
				 
			 }
			 else {final_wall = 3;
			 Strikeposition = (int) (game.ball[0].y - 600.0 *((Math.tan(game.ball[0].theta))));}
			 
			 if((game.ball[0].theta - (90*Ball.degToRad))< (3.0/2.0)*Math.atan(600.0/(600-game.ball[0].y))){
				 FakeStrikePos = 2;
				 moveDirection = -1;
			 }
			 }
		 
		 else if (wall%4 == 1 && game.ball[0].ya <0){
			 if((270*Ball.degToRad - game.ball[0].theta) < Math.atan(600.0/game.ball[0].y)){
				 final_wall = 0;
				 Strikeposition = (int) (600.0 - (game.ball[0].y/Math.tan(game.ball[0].theta)));
				 if(Math.atan(600.0/game.ball[0].y)/2.0 < (270*Ball.degToRad - game.ball[0].theta)){
					 FakeStrikePos =3;
					 moveDirection = -1;
				 }
			 }
			 else {final_wall = 3;
			 Strikeposition = (int) (game.ball[0].y - 600.0*(Math.tan(game.ball[0].theta)));
			 if((270*Ball.degToRad - game.ball[0].theta) < (3.0/2.0)*Math.atan(600.0/game.ball[0].y)){
				 FakeStrikePos = 0;
				 moveDirection = -1;
			 }
			 }
		 }
		 
		 else if(wall%4 == 2 && game.ball[0].xa >0){
			 if((360*Ball.degToRad - game.ball[0].theta) < Math.atan(600.0/(600-game.ball[0].x))){
				 final_wall = 1;
				 Strikeposition = (int) (600.0 - (game.ball[0].x - 600.0)*(Math.tan(game.ball[0].theta)));
				 if(Math.atan(600.0/(600-game.ball[0].x))/2.0 < (360*Ball.degToRad - game.ball[0].theta)){
					 FakeStrikePos = 0;
					 moveDirection = 1;
				 }
			 }
			 else {final_wall = 0;
			 Strikeposition = (int) (game.ball[0].x - 600.0/(Math.tan(game.ball[0].theta)));
			 if((360*Ball.degToRad - game.ball[0].theta) < (3.0/2.0)*Math.atan(600.0/(600-game.ball[0].x))){
				 FakeStrikePos = 1;
				 moveDirection = -1;
			 }
			 }
		 }
		 
		 else if(wall%4 == 2 && game.ball[0].xa <0){
			 if((game.ball[0].theta - 180*Ball.degToRad) < Math.atan(600.0/game.ball[0].x)){
				 final_wall = 3;
				 Strikeposition = (int) (600.0 - game.ball[0].x*(Math.tan(game.ball[0].theta)));
				 if(Math.atan(600.0/game.ball[0].x)/2.0 < (game.ball[0].theta - 180*Ball.degToRad)){
					 FakeStrikePos = 0;
					 moveDirection = -1;
				 }
			 }
			 else {final_wall = 0;
			 Strikeposition = (int) (game.ball[0].x - (600.0/(Math.tan(game.ball[0].theta))));
			 if((game.ball[0].theta - 180*Ball.degToRad) < (3.0/2.0)*Math.atan(600.0/game.ball[0].x)){
				 FakeStrikePos = 3;
				 moveDirection = -1;
			 }
			 }
		 }
		 
		 else if(wall%4 == 3 && game.ball[0].ya >0){
			 if((90*Ball.degToRad - game.ball[0].theta) < Math.atan(600.0/(600-game.ball[0].y))){
				 final_wall = 2;
				 Strikeposition = (int) ((600.0 - game.ball[0].y)/Math.tan(game.ball[0].theta));
				 
				 if(Math.atan(600.0/(600-game.ball[0].y))/2.0 < (90*Ball.degToRad - game.ball[0].theta)){
					 FakeStrikePos = 1;
					 moveDirection = 1;
				 }
			 }
			 else {final_wall =1;
			 Strikeposition = (int) (game.ball[0].y + 600.0*Math.tan(game.ball[0].theta));
			 if((90*Ball.degToRad - game.ball[0].theta) < (3.0/2.0)*Math.atan(600.0/(600-game.ball[0].y))){
				 FakeStrikePos = 2;
				 moveDirection = 1;
			 }
			 }
			 
		 }
		 
		 else if(wall%4 == 3 && game.ball[0].ya <0){
			if((game.ball[0].theta - 270*Ball.degToRad) < Math.atan(600.0/game.ball[0].y)){
				final_wall = 0;
				Strikeposition = (int) - (game.ball[0].y/Math.tan(game.ball[0].theta));
				if( Math.atan(600.0/game.ball[0].y)/2.0 < (game.ball[0].theta - 270*Ball.degToRad)){
					FakeStrikePos = 1;
					moveDirection = -1;
				}
			} 
			else {final_wall = 1;
			Strikeposition = (int) (game.ball[0].y + 600.0*Math.tan(game.ball[0].theta));
			if((game.ball[0].theta - 270*Ball.degToRad) < (3.0/2.0)*Math.atan(600.0/game.ball[0].y)){
				FakeStrikePos = 0;
				moveDirection = 1;
			}
			}
		 }
	 
 		return final_wall;
	}
 
	
	
}
