import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

//java play create 2 desperado
//java play connectTo 10.457.1.0 7777 sandy 2 
public class play {
	public static void main(String[] args) {
		
		//Handling initial UI
		SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
            	
            	String name = "";
            	name = JOptionPane.showInputDialog(null, null, "Name", -1);
            	while(name.isEmpty()){
            		name = JOptionPane.showInputDialog(null, "Can't enter empty name", "Name", JOptionPane.ERROR_MESSAGE);
            	}
            	
            	String[] player = {"1", "   2   ", "3", "4" };
            	int players = JOptionPane.showOptionDialog(null, "Number of Players", null,
                        JOptionPane.WARNING_MESSAGE, -1, null, player, null);
            	players++;
            	
            	if(players>=1 && players<=4){
            	
            	if(players==1){
            		String[] buttons = {"Single Ball", "Two Balls" };

                    int rc = JOptionPane.showOptionDialog(null, "Number of Balls", null,
                        JOptionPane.WARNING_MESSAGE, -1, null, buttons, buttons[0]);
                    rc++;
                    Game game = new Game(players);
                    Network network = new Network(game);
        			game.network = network;
        			network.create(name);
        			
        			game.frame.setResizable(false);
        			game.frame.add(game);
        			game.frame.pack();
        			game.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        			game.frame.setLocationRelativeTo(null);
        			game.frame.setVisible(true);
        			game.start();
        			
        			if(rc==2){
        				game.setBalls(2);
        			}
        			
            	}else{
            		
            	String[] buttons = {"Create Server", "Connect to Existing Server" };

                int rc = JOptionPane.showOptionDialog(null, "Start new server or connect to existing server", null,
                    JOptionPane.WARNING_MESSAGE, -1, null, buttons, buttons[0]);
    			
                if(rc==0){
                	Game game = new Game(players);
        			Network network = new Network(game);
        			game.network = network;
        			network.create(name);
        			
        			game.frame.setResizable(false);
        			game.frame.add(game);
        			game.frame.pack();
        			game.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        			game.frame.setLocationRelativeTo(null);
        			game.frame.setVisible(true);
        			game.start();
        			
        			String str = "Address: " + network.address + ", Port: " + network.port;
        			JOptionPane.showMessageDialog(null, "Server started at " + str, null, JOptionPane.INFORMATION_MESSAGE);
        			
                }else if(rc==1){
                	
                	Game game = new Game(players);
        			Network network = new Network(game);
        			game.network = network;
        			
                	String address = "";
                	address = JOptionPane.showInputDialog(null, "XXX.XXX.XXX.XXX:YYYY", "Address", -1);
                	while(name.isEmpty()){
                		name = JOptionPane.showInputDialog(null, "Can't enter empty name\nXXX.XXX.XXX.XXX:YYYY", "Address", JOptionPane.ERROR_MESSAGE);
                	}
        			int port = Integer.parseInt(address.substring(address.length()-4,address.length()));
        			address = address.substring(0,address.length()-5);
        			System.out.println(address);
        			network.connectTo(name, address, port);
        			
        			game.frame.setResizable(false);
        			game.frame.add(game);
        			game.frame.pack();
        			game.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        			game.frame.setLocationRelativeTo(null);
        			game.frame.setVisible(true);
        			game.start();
                }
            	}
                
            	}
                
            }
        });
		
		
		/*
		if(args[0].equals("create")){
			int players = Integer.parseInt(args[1]);
			String name = args[2];
			Game game = new Game(players);
			Network network = new Network(game);
			game.network = network;
			network.create(name);
			game.frame.setResizable(false);
			game.frame.add(game);
			game.frame.pack();
			game.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			game.frame.setLocationRelativeTo(null);
			game.frame.setVisible(true);
			game.start();
		}else if(args[0].equals("connectTo")){
			String address = args[1];
			int port = Integer.parseInt(args[2]);
			String name = args[3];
			int players = Integer.parseInt(args[4]);
			int port2  = Integer.parseInt(args[5]);
			
			Game game = new Game(players);
			Network network = new Network(game,port2);
			game.network = network;
			network.connectTo(name, address, port);
			game.frame.setResizable(false);
			game.frame.add(game);
			game.frame.pack();
			game.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			game.frame.setLocationRelativeTo(null);
			game.frame.setVisible(true);
			
			game.start();
		}*/
	}
}
