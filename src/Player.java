import java.awt.Color;
import java.awt.Rectangle;
import java.net.InetAddress;
import java.net.UnknownHostException;


public class Player {
	
	public final int i;
	public static final int playerSpeed=8,playerWidth=10,commonSize = (Game.width*Game.scale)/4;
	public int position,port,size = commonSize;
	public String name,address;
	public InetAddress ip;
	public int score;
	public boolean isConnected,isCPU,toSend;
	public Color color;
	public static Game game;
	public int lastSuccessMessage=0,sentMessages=0;
	
	public Player(int position, String name,int i){
		this.position = position;
		this.i = i;
		this.name = name;
		isConnected = false;
		toSend = false;
		score = 0;
		isCPU = false;
	}
	
	public void handleSuccessResponse(int lastSuccessMessage){
		this.lastSuccessMessage = lastSuccessMessage;
	}
	
	public void handleScore(boolean toIncrement){
		if(toIncrement){
			score++;
		}else if(!toIncrement){
			score--;
		}
		if(game.currentPlayer==i){
			sendScore();
		}else if(game.players==2 && game.currentPlayer == i-1){
			game.player[i-1].sendScore();
		}
	}
	
	public void sendScore(){
		if(game.players!=1){
		String message = "/sc/" + score + ",";
		if(game.players==2){
			message += game.player[i+1].score;
		}
		game.network.sendToAll(message);
		}
	}
	
	public void setIP(String address, int port){
		System.out.println(name + "-setIP-" + address + ":" + port);
		if(address.charAt(0)=='/'){
			this.address = address.substring(1,address.length());this.port = port;
		}else{
			this.address = address;this.port = port;
		}
		try {
			ip = InetAddress.getByName(address);
		} catch (UnknownHostException e) {	
			System.out.println("Player:" + name);
			e.printStackTrace();
		}
	}
	
	public Rectangle getBounds(){
		if(i==0){return new Rectangle(position-(size/2), 0, size, playerWidth);}
		if(i==1){return new Rectangle(Game.width*Game.scale - playerWidth, position-(size/2), playerWidth, size);}
		if(i==2){return new Rectangle(position-(size/2), Game.height*Game.scale - playerWidth, size, playerWidth);}
		return new Rectangle(0, position-(size/2), playerWidth, size);
	}

	public void checkConnection() {
		/*if(sentMessages-lastSuccessMessage>20){
			game.setCPU(i);
		}*/
	}

	public void move() {
		if(game.players != 2){			//giving only left,right or up, down controls
			if(i%2==0){
				if (Keyboard.left) {
					if(position - size/2 - playerSpeed >= playerWidth){
						position-=playerSpeed;game.updates++;
					}else{
						position = playerWidth + size/2;
					}
				}
				if (Keyboard.right) {
					if(position + size/2 + playerSpeed <= game.getWidth() - playerWidth){
						position+=playerSpeed;game.updates++;
					}else{
						position = game.getWidth() - playerWidth - size/2;
					}
				}
			}else{
				if (Keyboard.up) {
					if(position - size/2 - playerSpeed >= playerWidth){
						position-=playerSpeed;game.updates++;
					}else{
						position = playerWidth + size/2;
					}
				}
				if (Keyboard.down) {
					if(position + size/2 + playerSpeed <= game.getHeight()-playerWidth){
						position+=playerSpeed;game.updates++;
					}else{
						position = game.getHeight()-playerWidth - size/2;
					}
				}
			}
		}else{
			if (Keyboard.up) {
				if(game.player[i+1].position - game.player[i+1].size/2 - playerSpeed >= playerWidth){
					game.player[i+1].position-=playerSpeed;game.updates++;
				}else{
					game.player[i+1].position = playerWidth + size/2;
				}
			}
			if (Keyboard.down) {
				if(game.player[i+1].position + game.player[i+1].size/2 + playerSpeed <= game.getHeight()-playerWidth){
					game.player[i+1].position+=playerSpeed;game.updates++;
				}else{
					game.player[i+1].position = game.getHeight() - playerWidth - size/2;
				}
			}
			if (Keyboard.left) {
				if(position - size/2 - playerSpeed >= playerWidth){
					position-=playerSpeed;game.updates++;
				}else{
					position = playerWidth + size/2;
				}
			}
			if (Keyboard.right) {
				if(position + size/2 + playerSpeed <= game.getWidth() - playerWidth){
					position+=playerSpeed;game.updates++;
				}else{
					position = game.getWidth() - playerWidth - size/2;
				}
			}
		}
	}
	
}
