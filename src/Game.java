import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferStrategy;

import javax.swing.JFrame;

public class Game extends Canvas implements Runnable {

	private static final long serialVersionUID = 1L;
	public static final int width = 300,height = 300;
	public static final int scale = 2,playerWidth = Player.playerWidth;

	private static String title = "PingPong";
	private Thread thread;
	public JFrame frame;
	private boolean running = false;
	private Keyboard keyboard;
	private int balls = 1;
	public Ball[] ball = new Ball[2];
	public Network network;
	public Player player[] = new Player[4];	//topPlayer,rightPlayer,bottomPlayer,leftPlayer
	public int currentPlayer;	//0, 1, 2, 3
	public int players,connected_players = 1;		//total players playing excluding CPU mainly we want to just see if 2 players or not
	public boolean start = false;
	
	
	public Game(int players) {
		Player.game = this;
		Ball.setGame(this);
		Dimension size = new Dimension(width * scale, height * scale);
		setPreferredSize(size);
		frame = new JFrame();
		keyboard = new Keyboard();
		addKeyListener(keyboard);
		setFocusable(true);
		
		ball[0] = new Ball(150,150,Color.RED,0);
		ball[0].setTheta(Ball.degToRad*45.0);
		for(int i=0;i<4;i++){
			player[i] = new Player((width*scale)/2, "player" + i, i);
			player[i].color = Color.BLUE;
		}
		setPlayers(players);
		if(players==1){start=true;}
	}

	public void setBalls(int balls){
		this.balls = balls;
		if(balls==2){
			ball[1] = new Ball(300,200,Color.RED,1);
			ball[1].setTheta(Ball.degToRad*45.0);
		}
	}
	
	public void setPlayers(int players){
		this.players = players;
		System.out.println(players);
		if(players!=2){
			if(players==1){
				for(int i=players;i<4;i++){
					setCPU(i);
				}
			}else if(players==3){
				player[0].color = Color.RED;
				setCPU(3);
			}else{
				player[currentPlayer].color = Color.RED;
			}
		}else{
			player[0].color = Color.RED;
			player[1].color = Color.RED;
		}
	}
	
	public void setCPU(int i){
		if(players!=2){
			player[i] = new CPU((width*scale)/2, "CPU" + i, i);
			player[i].color = Color.GRAY;
		}
	}
	
	public void setPlayer(int i,String name){
		player[i] = new Player((width*scale)/2, "name", i);
		player[i].isConnected = true;
		player[i].isCPU = false;
		player[i].toSend = true;
		if(players==2){
			player[i+1] = new Player((width*scale)/2, "name", i+1);
			player[i].toSend = false;
		}
	}
	
	public void setCurrentPlayer(int currentPlayer){
		this.currentPlayer = currentPlayer%4;
		player[this.currentPlayer].isCPU = false;
	}
	
	public int getCurrentPlayer(){
		return currentPlayer;
	}
	
	public void updatePlayers(String message,int sender){
		String parts[] = message.split(",");
		int a = Integer.parseInt(parts[0]);
		if(players==2){
			int b = Integer.parseInt(parts[1]);
			int nextPlayer = (currentPlayer + 2)%4;
			System.out.println("Game.updatePlayers:" + nextPlayer +", "+ a +", "+ b);
			player[sender].position = a;
			player[sender+1].position = b;
		}else{
			player[sender].position = a;
		}
	}
	
	public void setColors(){
		if(players == 2){
			player[currentPlayer].color = Color.RED;
			player[(currentPlayer+1)%4].color = Color.RED;
			player[(currentPlayer+1)%4].name = player[currentPlayer].name;
			player[(currentPlayer+1)%4].isCPU = player[currentPlayer].isCPU;
			player[(currentPlayer+1)%4].toSend = player[currentPlayer].toSend;
			player[(currentPlayer+2)%4].color = Color.BLUE;
			player[(currentPlayer+3)%4].color = Color.BLUE;
		}else{
			player[currentPlayer].color = Color.RED;
			int i = (currentPlayer+1)%4;
			if(!player[i].isCPU){player[i].color = Color.BLUE;}
			else {player[i].color = Color.GRAY;}
			i = (currentPlayer+2)%4;
			if(!player[i].isCPU){player[i].color = Color.BLUE;}
			else {player[i].color = Color.GRAY;}
			i = (currentPlayer+3)%4;
			if(!player[i].isCPU){player[i].color = Color.BLUE;}
			else {player[i].color = Color.GRAY;}
		}
	}
	
	public void connectUser(String name,String address, int port){
		if(connected_players==players){
			String message = "/er/" + "Server is already full" + "/e/";
			network.send(message.getBytes(),address,port);
			return;
		}
		if(players==2){
			player[2].setIP(address, port);
			player[2].name = name;
			player[2].isConnected = true;
			player[2].isCPU = false;
			player[3].isCPU = false;
			player[2].toSend = true;
			connected_players++;
			String message = "/s/2"+player[currentPlayer].name+"/e/";
			network.send(message.getBytes(), 2);
			start = true;
			return;
		}
		if(players==4 || players==3){
			player[connected_players].setIP(address, port);
			player[connected_players].name = name;
			player[connected_players].isConnected = true;
			player[connected_players].isCPU = false;
			player[connected_players].toSend = true;
			String message = "/s/"+connected_players+player[currentPlayer].name+"/e/";
			network.send(message.getBytes(), connected_players);
			message = "/a/"+connected_players+player[connected_players].name;
			message += "/w/"+player[connected_players].address+"/w/"+player[connected_players].port+"/e/";
			for(int i=1;i<connected_players;i++){
				network.send(message.getBytes(), i);
			}
			for(int i=1;i<connected_players;i++){
				message = "/a/"+i+player[i].name;
				message += "/w/"+player[i].address+"/w/"+player[i].port+"/e/";
				network.send(message.getBytes(), connected_players);
			}
			connected_players++;
			if(connected_players==players){
				start = true;
			}
			return;
		}
	}
	
	public void addPlayer(String message){
		int i = message.charAt(0) - '0';
		String parts[] = message.split("/w/");
		String name = parts[0].substring(1,parts[0].length());
		String address = parts[1];
		int port = Integer.parseInt(parts[2]);
		player[i].name = name;
		player[i].setIP(address, port);
		player[i].toSend = true;
		player[i].isCPU = false;
		player[i].isConnected = true;
		connected_players++;
		if(connected_players==players){
			start = true;
		}
	}
	
	public void onConnected(int position,String name,String address, int port){
		System.out.println("Game.onConnected:"+ position + ", " + name +", "+ address +":"+ port );
		setCurrentPlayer(position);
		player[position].color = Color.RED;
		player[position].name = network.name;
		player[position].isConnected = true;
		player[position].isCPU = false;
		player[position].toSend = false;
		player[position].setIP(network.address, network.port);
		player[0].name = name;
		player[0].isConnected = true;
		player[0].isCPU = false;
		player[0].toSend = true;
		player[0].setIP(address, port);
		connected_players++;
		if(players==2){
			player[position+1].toSend = false;
			player[1].toSend = false;
			start = true;
		}
		setColors();
	}

	public synchronized void start() {
		running = true;
		thread = new Thread(this, "Display");
		thread.start();
	}

	public synchronized void stop() {
		running = false;
		try {
			thread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public void run() {
		double updates_ns = 1000000000.0 / 60.0,render_ns = 1000000000.0 / 60.0;
		double updates_delta = 0,render_delta=0;

		int frames = 0;
		int updates = 0;

		long lastTime = System.nanoTime();
		long timer = System.currentTimeMillis();



		while (running) {
			long now = System.nanoTime();

			updates_delta += (now - lastTime) / updates_ns;
			render_delta += (now - lastTime) / render_ns;
			lastTime = now;

			while (updates_delta >= 1) {
				update();
				updates++;
				updates_delta--;
			}

			while (render_delta >= 1) {
				render();
				frames++;
				render_delta--;
			}

			if (System.currentTimeMillis() - timer >= 1000) {
				timer += 1000;
				frame.setTitle(title + "  |  " + updates + " ups, " + frames
						+ " fps");
				frames = 0;
				updates = 0;
			}
		}
	}

	public void moveCPU(){
		for(int i=0;i<4;i++){
			if(player[i].isCPU){
				player[i].move();
			}
		}
	}
	
	public int updates=0;
	
	public void moveBalls(){
		ball[0].move();
		if(balls==2){
			ball[1].move();
		}
	}
	
	public void update() {
		keyboard.update();
		if(start){
			moveBalls();
			player[currentPlayer].move();
			moveCPU();
		}

		if(updates>=2){
			updates=0;
			String message = "/p/" + player[currentPlayer].position + ",";
			if(players==2) message += player[currentPlayer+1].position;
			network.sendToAll(message);
		}
	}
	
	public int wall = -1;
	private void glow(Graphics g){
		if(wall!=-1){
			g.setColor(Color.MAGENTA);
			int i = wall-4;
			if(i==0){
				g.fillRect(playerWidth, 0, player[i].position-player[i].size/2, playerWidth/2);
				g.fillRect(player[i].position+player[i].size/2, 0, getWidth()-playerWidth-player[i].position+player[i].size/2, playerWidth/2);
			}else if(i==1){
				g.fillRect(getWidth()-playerWidth/2, playerWidth, playerWidth/2, player[i].position-player[i].size/2);
				g.fillRect(getWidth()-playerWidth/2, player[i].position+player[i].size/2, playerWidth/2, getHeight()-playerWidth-player[i].position+player[i].size/2);
			}else if(i==2){
				g.fillRect(playerWidth, getHeight()-playerWidth/2, player[i].position-player[i].size/2, playerWidth/2);
				g.fillRect(player[i].position+player[i].size/2, getHeight()-playerWidth/2, getWidth()-playerWidth-player[i].position+player[i].size/2, playerWidth/2);
			}else if(i==3){
				g.fillRect(0, playerWidth, playerWidth/2, player[i].position-player[i].size/2);
				g.fillRect(0, player[i].position+player[i].size/2, playerWidth/2, getHeight()-playerWidth-player[i].position+player[i].size/2);
			}
			
		}	
	}
	
	private Font font = new Font("Bitmap", Font.PLAIN, 25);
	public void renderScore(Graphics g){
		int i=0;
		if(!player[i].isCPU){g.drawString(String.valueOf(player[i].score), getWidth()/2, playerWidth+30);}
		i=2;
		if(!player[i].isCPU){g.drawString(String.valueOf(player[i].score), getWidth()/2,getHeight()- playerWidth-10);}
		i=3;
		if(!player[i].isCPU){g.drawString(String.valueOf(player[i].score), playerWidth+10, getHeight()/2);}
		i=1;
		if(!player[i].isCPU){g.drawString(String.valueOf(player[i].score), getWidth()-playerWidth-30, getHeight()/2);}
	}
	
	public void renderBalls(Graphics g){
		g.setColor(ball[0].color);
		g.fillOval(ball[0].x, ball[0].y, Ball.DIAMETER, Ball.DIAMETER);	//coordinates of rectangles top left corner, width is overall not from center
		if(balls==2){
			g.setColor(ball[1].color);
			g.fillOval(ball[1].x, ball[1].y, Ball.DIAMETER, Ball.DIAMETER);
		}
	}
	
	public void render() {
		BufferStrategy bs = getBufferStrategy();
		if (bs == null) {
			createBufferStrategy(3);
			return;
		}

		Graphics g = bs.getDrawGraphics();
		g.clearRect(0, 0, getWidth(), getHeight());

			//Filling corners with gray color
		glow(g);
		
		g.setColor(Color.GRAY);
		g.fillRect(0, 0, playerWidth, playerWidth);
		g.fillRect(0, getWidth()- playerWidth, playerWidth, playerWidth);
		g.fillRect(getWidth()- playerWidth, 0, playerWidth, playerWidth);
		g.fillRect(getWidth()- playerWidth, getWidth()- playerWidth, playerWidth, playerWidth);
		
			//Rendering Players with appropriate colors and size
		g.setColor(player[0].color);
		g.fillRect(player[0].position-(player[0].size/2), 0, player[0].size, playerWidth);
		g.setColor(player[1].color);
		g.fillRect(getWidth() - playerWidth, player[1].position-(player[1].size/2), playerWidth, player[1].size);
		g.setColor(player[2].color);
		g.fillRect(player[2].position-(player[2].size/2), getHeight() - playerWidth, player[2].size, playerWidth);
		g.setColor(player[3].color);
		g.fillRect(0, player[3].position-(player[3].size/2), playerWidth, player[3].size);
		Graphics2D g2 = (Graphics2D) g;
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2.setFont(font);
		renderScore(g);
		renderBalls(g);
		g.dispose();
		bs.show();
	}
}