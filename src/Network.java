import java.awt.event.WindowEvent;
import java.io.IOException;
import java.net.BindException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Collections;
import java.util.Enumeration;

import javax.swing.JOptionPane;


public class Network implements Runnable{
	
	public boolean connected;
	private DatagramSocket socket;
	public String name;
	public String address = "localhost";
	public int port = 7777;
	private boolean running = false;
	private Thread startThread, receiveThread, sendThread;
	private Game game;
	private InetSocketAddress addr;
	
	public Network(Game game){
		this.game = game;
		// Currently not checking for whether to take wlan or ethernet
		address = findIP();
		if(address.isEmpty()){
			System.err.println("Cannot find a working IP address");
			return;
		}
		boolean bound = false;
		while(!bound){
		    try {
		    	addr = new InetSocketAddress(address, port);
		    	socket = new DatagramSocket(null);
				socket.bind(addr);
				bound = true;
			} catch (BindException e) {
				port++;
			} catch (SocketException e) {
				port++;
			}
		}
	    startThread = new Thread(this, "Network - startThread");
	    startThread.start();
	}
	
	public void create(String name){
		game.setCurrentPlayer(0);
		game.player[0].name = name;
	}
	
	private String findIP(){
		String retString = "";
		try {
			Enumeration<NetworkInterface> nets = NetworkInterface.getNetworkInterfaces();
			for (NetworkInterface netint : Collections.list(nets)){
				if(netint.isLoopback() || !netint.isUp()) continue;
				Enumeration<InetAddress> inetAddresses = netint.getInetAddresses();
				for (InetAddress inetAddress : Collections.list(inetAddresses)) {
		        	if(isIPv4(inetAddress.toString().substring(1))){
		        		retString = inetAddress.toString().substring(1);
		        	}
		        }
			}
		} catch (SocketException e) {
			e.printStackTrace();
		}
		return retString;
	}
	
	private boolean isIPv4(String ip){
		try {
	        if ( ip == null || ip.isEmpty() ) {
	            return false;
	        }

	        String[] parts = ip.split( "\\." );
	        if ( parts.length != 4 ) {
	            return false;
	        }

	        for ( String s : parts ) {
	            int i = Integer.parseInt( s );
	            if ( (i < 0) || (i > 255) ) {
	                return false;
	            }
	        }
	        if ( ip.endsWith(".") ) {
	            return false;
	        }

	        return true;
	    } catch (NumberFormatException nfe) {
	        return false;
	    }
	}
	
	public void run(){
		running = true;
		String str = "Address: " + address + ", Port: " + socket.getLocalPort();
		System.out.println(str);
		receive();
	}
	
	public void connectTo(String name, String address, int port){
		this.name = name;
		String message= "/c/" + name + "/e/";
		send(message.getBytes(),address,port);
	}
	
	private void receive() {
		receiveThread = new Thread("Receive") {
			public void run() {
				while (running) {
					byte[] data = new byte[1024];
					DatagramPacket packet = new DatagramPacket(data, data.length);
					try {
						socket.receive(packet);
					} catch (IOException e) {
						//e.printStackTrace();
					}
					String message = new String(packet.getData());
					message = message.split("/e/")[0];		//have to consider length of messageString
					//System.out.println(message);
					String address = packet.getAddress().toString();address = address.substring(1,address.length());
					extractSemantics(message,address,packet.getPort());
				}
			}
		};
		receiveThread.start();
	}
	
	private void extractSemantics(String message, String address, int port){
		//System.out.println("Network.extractSemantics:"+ message + ", " + address +":"+ port );
		if(message.startsWith("/b/")){
			String parts[] = message.split("/t/");
			int sender = Integer.parseInt(parts[1].split(",")[0]);
			int messageNumber = Integer.parseInt(parts[1].split(",")[1]);
			String successString = "/sr/"+ messageNumber +"/t/" + game.currentPlayer + "/e/";
			send(successString.getBytes(), sender);
			parts = parts[0].substring(3,parts[0].length()).split("/w/");
			double theta = Double.parseDouble(parts[0]);
			int x = Integer.parseInt(parts[1]);
			int y = Integer.parseInt(parts[2]);
			double tx = Double.parseDouble(parts[3]);
			double ty = Double.parseDouble(parts[4]);
			//System.out.println("Network.extractSemantics: " + /b/: " + x + ", " + y + ", " + theta);
			game.ball[0].updateBall(theta,x,y,tx,ty);
			return;
		}
		if(message.startsWith("/p/")){
			String parts[] = message.split("/t/");
			int sender = Integer.parseInt(parts[1].split(",")[0]);
			int messageNumber = Integer.parseInt(parts[1].split(",")[1]);
			String successString = "/sr/"+ messageNumber +"/t/" + game.currentPlayer + "/e/";
			send(successString.getBytes(), sender);
			game.updatePlayers(parts[0].substring(3,parts[0].length()),sender);
			return;
		}
		if(message.startsWith("/sc/")){
			message = message.split("/sc/")[1];
			String parts[] = message.split("/t/");
			int sender = Integer.parseInt(parts[1].split(",")[0]);
			int messageNumber = Integer.parseInt(parts[1].split(",")[1]);
			String successString = "/sr/"+ messageNumber +"/t/" + game.currentPlayer + "/e/";
			send(successString.getBytes(), sender);
			int score1 = Integer.parseInt(parts[0].split(",")[0]);
			game.player[sender].score = score1;
			if(game.players==2){
				game.player[sender+1].score = Integer.parseInt(parts[0].split(",")[1]);
			}
			return;
		}
		if(message.startsWith("/c/")){
			game.connectUser(message.substring(3, message.length()),address,port);
			return;
		}
		if(message.startsWith("/s/")){
			game.onConnected(message.charAt(3) - '0',message.substring(4, message.length()),address,port);
			return;
		}
		if(message.startsWith("/sr/")){
			String parts[] = message.split("/t/");
			int sender = Integer.parseInt(parts[1]);
			int lastSuccessMessage = Integer.parseInt(parts[0].substring(4,parts[0].length()));
			game.player[sender].handleSuccessResponse(lastSuccessMessage);
			return;
		}
		if(message.startsWith("/er/")){
			running = false;
			JOptionPane.showMessageDialog(null, message.substring(4,message.length()), null, JOptionPane.INFORMATION_MESSAGE);
			game.frame.dispatchEvent(new WindowEvent(game.frame, WindowEvent.WINDOW_CLOSING));
			return;
		}
		if(message.startsWith("/a/")){
			game.addPlayer(message.substring(3,message.length()));
			return;
		}
		System.out.println("Command not defined");
	}
	
	
	public void sendToAll(String message){
		for(int i=0;i<4;i++){
			if(game.player[i].toSend){
				send((message + "/t/" + game.currentPlayer + "," + ++game.player[i].sentMessages + "/e/").getBytes(),i);
				game.player[i].checkConnection();
			}
		}
	}

	public void send(final byte[] data, final String ip, final int port){
		//System.out.println(new String(data));
		sendThread = new Thread("ClientSend"){
			public void run() {
				DatagramPacket packet;
				try {
					System.out.println(ip +", " + port);
					packet = new DatagramPacket(data,data.length,InetAddress.getByName(ip),port);
					socket.send(packet);
				} catch (UnknownHostException e1) {
					e1.printStackTrace();
				}catch (IOException e) {
					e.printStackTrace();
				}
			}
		};
		sendThread.start();
	}
	
	//sending to player i
	public void send(final byte[] data,final int i){
		//System.out.println(new String(data));
		sendThread = new Thread("ClientSend" + i){
			public void run() {
				
				DatagramPacket packet = new DatagramPacket(data,data.length,game.player[i].ip,game.player[i].port);
				try{
					socket.send(packet);
				}catch (IOException e) {
					e.printStackTrace();
				}
			}
		};
		sendThread.start();
	}

}
