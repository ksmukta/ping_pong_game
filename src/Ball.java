import java.awt.Color;
import java.awt.Rectangle;


public class Ball {
	
	public double theta; 			// with the x axis, clockwise positive
	public int x,y;					//position of ball
	public double delta = 3;		//incremental angle
	public int deltaMultiplier = 10,i;	
	public double xa,ya,tx,ty;		//increment to ball and exact position without integer rounding
	private int speed = 100;
	public final static double epsilon = 0.1, degToRad = Math.atan(1)*4.0/180.0;
	public Color color = Color.GREEN;
	public static final int DIAMETER = 20,RADIUS = DIAMETER/2;
	public static Game game;
	
	public Ball(int x, int y,Color color,int i){
		this.x = x;
		this.y = y;
		tx = x; ty = y;
		this.color = color;
	}
	
	public static void setGame(Game game){
		Ball.game = game;
	}
	
	public void setTheta(double theta){
		if(theta>360.0*degToRad){
			setTheta(theta - 360.0*degToRad);
			return;
		}
		this.theta = theta;
		xa = Math.sqrt(speed)*Math.cos(theta);
		ya = Math.sqrt(speed)*Math.sin(theta);
		//System.out.println(theta/degToRad);
	}
	
	public void setSpin(int player, boolean direction){
		if(Keyboard.shiftPressed){
			boolean positive=true,pressed=false;
			if(player%2==0){
				if(direction){
					if(Keyboard.left){positive=false;pressed=true;}
					else if(Keyboard.right){positive=true;pressed=true;}
				}else{
					if(Keyboard.left){positive=true;pressed=true;}
					else if(Keyboard.right){positive=false;pressed=true;}
				}
			}else{
				if(direction){
					if(Keyboard.up){positive=false;pressed=true;}
					else if(Keyboard.down){positive=true;pressed=true;}
				}else{
					if(Keyboard.up){positive=true;pressed=true;}
					else if(Keyboard.down){positive=false;pressed=true;}
				}
			}
			if(pressed){
				if(positive){
					setTheta(this.theta + (deltaMultiplier*delta*degToRad));
				}else{
					setTheta(this.theta - (deltaMultiplier*delta*degToRad));
				}
				String message = "/b/" + theta + "/w/" + x + "/w/" + y + "/w/" + tx + "/w/" + ty;
				game.network.sendToAll(message);
			}
		}
	}
	
	public void updateBall(double theta, int x, int y,double tx, double ty){
		setTheta(theta);
		this.x = x;this.tx = tx;
		this.y = y;this.ty = ty;
	}
	
	public void move(){
		int collided = collision();
		if(collided!=-1){
			boolean direction;
			if(collided%2==0){
				setTheta(degToRad*360.0 - theta);
				direction = xa>0;
			}else{
				setTheta(degToRad*540.0 - theta);
				direction = ya>0;
			}
			if(collided==game.currentPlayer || (collided==game.currentPlayer+1 && game.players==2)){
				setSpin(collided, direction);
			}
			CPU.playerToMove = CPU.intersection(collided);
			//System.out.println(x + ", " + y);
			//System.out.println("Collided with: " + collided%4 + " ----  Collision.prediction: " + CPU.intersection(collided) + " ----  Strike.prediction: " + CPU.Strikeposition + "------- FakeStrikePosition: " + CPU.FakeStrikePos + "-------moveDirection" + CPU.moveDirection);
		}
		tx += xa;
		ty += ya;
		if((int) tx - epsilon != (int) tx ){
			x = (int) tx;
		}else if((int) tx + epsilon != (int) tx){
			x = (int) (tx + epsilon);
		}
		if((int) ty - epsilon != (int) ty ){
			y = (int) ty;
		}else if((int) ty + epsilon != (int) ty){
			y = (int) (ty + epsilon);
		}
	}
	
	public void glow(final int i){
		Thread glowThread = new Thread("glowThread"){
			public void run() {
				game.wall = i;
				try {
					Thread.sleep(50);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				game.wall = -1;
			};
		};
		glowThread.start();
	}
	
	private static int lastCollided=-1;//,lastCollidedBall=-1;
	public int collision(){
		Rectangle ball = getBounds();
		for(int i=0;i<4;i++){
			if(game.player[i].getBounds().intersects(ball)){
				if(lastCollided!=(i%4)){
					game.player[i].handleScore(true);
					lastCollided = i;
					return i;
				}
			}
		}
		/*for(int i=0;i<4;i+=2){
			if(game.player[i].getBounds().intersects(ball)){
				int playerPosition = game.player[i].position;
				if((i==0 && y>=Player.playerWidth) || (i==2 && (y+Ball.DIAMETER)<= game.getHeight() - Player.playerWidth)){
					if(x<playerPosition){
						if(x+RADIUS >= playerPosition-Player.commonSize/2){
							if(game.currentPlayer==i){game.player[i].handleScore(true);}
							return i;
						}
					}else{
						if(x+RADIUS <= playerPosition+Player.commonSize/2){
							if(game.currentPlayer==i){game.player[i].handleScore(true);}
							return i;
						}
					}
				}
			}
		}
		
		for(int i=1;i<4;i+=2){
			if(game.player[i].getBounds().intersects(ball)){
				int playerPosition = game.player[i].position;
				if(i==3){
					System.out.println((y+Ball.RADIUS) + ", " + (playerPosition-Player.commonSize/2));
				}
				if((i==3 && x+xa>=Player.playerWidth) || (i==1 && (x+xa+Ball.DIAMETER)<= game.getWidth() - Player.playerWidth)){	
					if(y<playerPosition){
						if(y+RADIUS >= playerPosition-Player.commonSize/2){
							if(game.currentPlayer==i || (game.players==2 && game.currentPlayer==i-1)){game.player[i].handleScore(true);}
							return i;
						}
					}else{
						if(y+RADIUS <= playerPosition+Player.commonSize/2){
							if(game.currentPlayer==i || (game.players==2 && game.currentPlayer==i-1)){game.player[i].handleScore(true);}
							return i;
						}
					}
				}
			}
		}*/
		int x = (int)(this.x+ xa);
		if(x<0){
			if(game.currentPlayer==3 || (game.currentPlayer==2 && game.players==2)){game.player[3].handleScore(false);}
			glow(7);
			lastCollided = 7;
			return 7;
			}
		if(x>(Game.width*Game.scale-DIAMETER)){
			if(game.currentPlayer==1 || (game.currentPlayer==0 && game.players==2)){game.player[1].handleScore(false);}
			glow(5);
			lastCollided = 5;
			return 5;
			}
		int y = (int)(this.y+ ya);
		if(y<0){
			if(game.currentPlayer==0){game.player[0].handleScore(false);}
			glow(4);
			lastCollided = 4;
			return 4;
		}
		if(y>(Game.height*Game.scale-DIAMETER)){
			if(game.currentPlayer==2){game.player[2].handleScore(false);}
			glow(6);
			lastCollided = 6;
			return 6;
			}
		return -1;
	}
	
	public Rectangle getBounds(){
		return new Rectangle((int)(x+xa), (int)(y+ya), DIAMETER, DIAMETER);
	}
	public double getXA(){
		return xa;
	}
	public double getYA(){
		return ya;
	}
}
